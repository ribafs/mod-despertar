# Módulo pensamentos para o despertar
## Suporta Joomla 2.5 e 3.x

Pequeno módulo que mostra um pensamento aleatório a cada visita. São vários pensamentos, recomendações e versículos selecionados para colaborar com o despertar da consciência.

## Instalação
https://github.com/ribafs/pensamento-para-despertar

Instale como qualquer módulo apra Joomla 3.

## Demonstração Online
http://despertando.net

## Artigo com todas as Recomendações
Para que possa oferecer todas as recomendações no link precisará criar um artigo contendo todo o con teúdo do arquivo mod_despertar.txt. Claro que precisará formatar seu conteúdo, que está em texto claro. Sugestão: insira um hífen no início de cada recomendação e uma linha em branco separando cada uma. Pode ver como está no demo (link acima).

Lembre sempre de atualizar o artigo caso adicione outras recomendações, pensamentos ou versículos.
