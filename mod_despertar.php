<?php
/**
* @version $Id:  mod_pensamentos.php, V1.0.0  2009-01-01 $
* @module Joomla
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
*
* Adaptado do módulo vivi_code do Serra Antonio (tonio@vivigrosseto.it)
* http://www.vivigrosseto.it/ 
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$todos = $params->get('todos');
$download = $params->get('download');
$artigo = $params->get('artigo');

class recomendacoes
{
	function recomendacao_aleatoria(){
		$f_contents = file ('modules/mod_despertar/mod_despertar.txt');
		srand ((double)microtime()*1000000);
		$linha_aleatoria = $f_contents[ rand (0, (count ($f_contents) - 1)) ];
		return $linha_aleatoria;
	}
}
$aleatorio = new recomendacoes();
print '<div id="deseprtar">'.$aleatorio->recomendacao_aleatoria().'</div>'; 

if($todos){
?>
<br><a href="<?=$artigo;?>">Visualizar todas</a><br>
<?php
}
if($download){
?>
<a href="https://github.com/ribafs/mod-despertar" target="_blank" title="Download deste módulo"><span style="font-size:10px"><i>Download</i></span></a>
<?php
}

